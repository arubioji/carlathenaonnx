// CARLAthena includes
#include "CARLAthenaAlg.h"
#include "PathResolver/PathResolver.h"
//#include "GaudiKernel/PathResolver.h"

//#include "xAODEventInfo/EventInfo.h"
std::ostream& operator<<( std::ostream& s, std::pair<double, double > p )
{
  s << p.first << " " << p.second;
  return s;
}

// Base Constructor:
//       Base constructor of CARL re-weighting machinery
CARLAthenaAlg::CARLAthenaAlg( const std::string& name,
			      ISvcLocator* pSvcLocator )
  : AthAnalysisAlgorithm( name, pSvcLocator )
{

  // User specified flags
  declareProperty( "SampleName", m_sampleName = "Unknown",
                   "Descriptive name for the processed sample" );
  declareProperty("DoSherpaVpT"     , m_DoSherpaVpT = false);
  declareProperty("DoSherpattbar"   , m_DoSherpattbar = true);
  declareProperty("DoInference"     , m_DoInference = true);
  declareProperty("DoDebug"         , m_DoDebug = true);

}


CARLAthenaAlg::~CARLAthenaAlg() {}

void CARLAthenaAlg::ClearBranches() {

  m_MET = -999;
  m_Njets = -999;
  m_MET = -999;
  m_HT = -999;
  m_VpT = -999;
  m_Veta = -999;
  m_weight = -999;
  m_Jet_Pt.clear();
  m_Jet_Eta.clear();
  m_Jet_Phi.clear();
  m_Jet_Mass.clear();
  m_Lepton_Pt.clear();
  m_Lepton_Eta.clear();
  m_Lepton_Phi.clear();
  m_Lepton_Mass.clear();
  m_Lepton_ID.clear();
  m_nSignalLeptons = -1;

}


StatusCode CARLAthenaAlg::initialize() {
  if(m_DoDebug){ ATH_MSG_INFO ("Initializing " << name() << "...");}
  // Obtain the working area
  //DataPath = getenv("DATAPATH");
  
  // Register output trees
  ServiceHandle<ITHistSvc> histSvc("THistSvc",name());
  CHECK( histSvc.retrieve() );

  // Make tree
  TString TreeName = "Tree";
  Tree = new TTree(TreeName,"");
  Tree->Branch("Njets",&m_Njets);
  Tree->Branch("MET",&m_MET);
  Tree->Branch("HT",&m_HT);
  Tree->Branch("VpT",&m_VpT);
  Tree->Branch("Veta",&m_Veta);
  Tree->Branch("Lepton_Pt",&m_Lepton_Pt);
  Tree->Branch("Lepton_Eta",&m_Lepton_Eta);
  Tree->Branch("Lepton_Phi",&m_Lepton_Phi);
  Tree->Branch("Lepton_Mass",&m_Lepton_Mass);
  Tree->Branch("Lepton_ID",&m_Lepton_ID);
  Tree->Branch("nSignalLeptons",&m_nSignalLeptons);
  Tree->Branch("Jet_Pt",&m_Jet_Pt);
  Tree->Branch("Jet_Eta",&m_Jet_Eta);
  Tree->Branch("Jet_Phi",&m_Jet_Phi);
  Tree->Branch("Jet_Mass",&m_Jet_Mass);





  if(m_DoInference){
    //--------------------------------------------------------
    //Modular version
    //std::string model_path = PathResolver::find_file ("CARLAthena/CKKW20_carl_100000_new.onnx", "DATAPATH");
    std::string model_path = PathResolver::find_file ("CARLAthena/CKKW20_carl_1000000_new.onnx", "DATAPATH");
    m_CARLNN = new CARLNN(model_path, &m_input_tensor_values, m_DoDebug);
    //--------------------------------------------------------
    Tree->Branch("weight",&m_weight);
  }
  CHECK( histSvc->regTree("/test/tree_Tree", Tree) );
  
  return StatusCode::SUCCESS;
}

StatusCode CARLAthenaAlg::finalize() {
  if(m_DoDebug){ ATH_MSG_INFO ("Finalizing " << name() << "...");}

  // Delete the environment object.
  if(m_DoInference){
    //----------------------------------------------------
    //Modular version
    CHECK( m_CARLNN->finalise() );
    //delete m_CARLNN;
    //----------------------------------------------------
    
  }
  if(m_DoDebug){ ATH_MSG_DEBUG( "Ort::Env object deleted" );}

  return StatusCode::SUCCESS;
}

// User Code
TLorentzVector CARLAthenaAlg::GetDecayProducts(const xAOD::TruthParticle* mother, std::set<int> * W_decay_products, bool W_flag, TLorentzVector W_Vec){

const xAOD::TruthVertex* decvtx = mother->decayVtx();
if(decvtx){
  const unsigned int n_children = decvtx->nOutgoingParticles();
  for (unsigned int child=0; child < n_children; child++) {
    const xAOD::TruthParticle* daughter = decvtx->outgoingParticle(child);
    if(daughter == nullptr) {
  continue;
    }
    if(daughter->absPdgId() == 5) continue;
    if(daughter->absPdgId() == 21) continue;
    if(daughter->absPdgId() == 22) continue;
    if(!daughter->isW() && !daughter->isLepton() && !daughter->isTop()) continue;

    if(mother->isW() && daughter->isLepton()){
  W_flag = true;
  if(W_Vec.Pt() == 0){
    W_Vec = mother->p4();
  }
    }
    if(W_flag) W_decay_products->insert(abs(daughter->pdgId()));

    if(!W_flag) W_Vec = GetDecayProducts(daughter, W_decay_products, W_flag, W_Vec);
  }
}

return W_Vec;

}


StatusCode CARLAthenaAlg::execute() {
  if(m_DoDebug){ ATH_MSG_DEBUG ("Executing " << name() << "...");}
  ClearBranches();

  CHECK( evtStore()->retrieve( truthMuons, "TruthMuons" ) );
  CHECK( evtStore()->retrieve( truthElectrons, "TruthElectrons" ) );

  //Get jets and HT
  SignalJets_Vec.clear();
  SignalJets.clear();
  CHECK( evtStore()->retrieve( truthJets, "AntiKt4TruthJets" ) );
  //for(unsigned int j = 0; j < truthJets->size(); ++j) {
  for(const xAOD::Jet* jet : (*truthJets)) {
    
    //Check for minimum jet pT of 20GeV
    if( jet->p4().Pt() < 20e3 ) {
      continue;
    }
    //Now load if meeting eta requirements
    if( fabs(jet->eta()) < 2.5 ) {
      SignalJets.push_back( std::make_shared<xAOD::Jet>( *jet  ) );
      //SignalJets.back()->setJetP4( jet->jetP4() );

    }    
    
    //Old jet extraction, moved to new shared_ptr to ensure no memory leak in a 
    // elegant fashion. - See deletion fo this style later
    ////const xAOD::Jet * jet = (*truthJets)[j];
    //TLorentzVector JetVec;
    //JetVec = jet->p4();
    //
    //xAOD::JetFourMom_t JetMom(JetVec.Pt(),JetVec.Eta(),JetVec.Phi(),JetVec.M());
    ////std::unique_ptr<xAOD::Jet> calibjet(new xAOD::Jet());  //= std::make_unique<xAOD::Jet>();
    //xAOD::Jet * calibjet = new xAOD::Jet;
    //calibjet->makePrivateStore();
    //calibjet->setJetP4(JetMom);
    //
    //if( calibjet->pt() < 20e3 ) {
    //  delete calibjet; //Not needed with unique pointers
    //  continue;
    //}
    //if( fabs(calibjet->eta()) < 2.5 && calibjet->pt() > 20e3 ) {
    //
    //  SignalJets.push_back(calibjet);
    //  //SignalJets.push_back(std::move(calibjet));
    //
    //}
  }

  //Sort to decending pt ordering
  std::sort(SignalJets.begin(),SignalJets.end(),sort_pt);

  //Construct Scalar sum of pT
  double HT = 0;
  for(std::shared_ptr<xAOD::Jet> jet : (SignalJets)){

    //std::unique_ptr<xAOD::Jet> jet = SignalJets.at(j);
    //xAOD::Jet * jet = SignalJets.at(j);

    TLorentzVector Jet_Vec; // in GeV
    Jet_Vec.SetPtEtaPhiM(jet->pt()*1e-3,jet->eta(),jet->phi(),jet->m()*1e-3);
    SignalJets_Vec.push_back(Jet_Vec);
    HT += jet->pt()*1e-3;
  }
  m_HT = HT;



  for(unsigned int j = 0; j < SignalJets_Vec.size(); ++j) {
    m_Jet_Pt.push_back( SignalJets_Vec.at(j).Pt() );
    m_Jet_Eta.push_back( SignalJets_Vec.at(j).Eta() );
    m_Jet_Phi.push_back( SignalJets_Vec.at(j).Phi());
    m_Jet_Mass.push_back( SignalJets_Vec.at(j).M() );
  }
  m_Njets = SignalJets_Vec.size();

  CHECK( evtStore()->retrieve( truthParticles, "TruthParticles" ));
  TLorentzVector final_V;
  TLorentzVector temp_final_V;
  final_V.SetPtEtaPhiE(0, 0, 0, 0);
  int nLep = 0;
  int nAntiLep = 0;

  for(unsigned int t = 0; t < truthParticles->size(); ++t) {
    const xAOD::TruthParticle * particle = (*truthParticles)[t];
    if(m_DoSherpaVpT){
      if( particle->status() == 3 ){    //Getting truth boson information (for Sherpa)
        if( ! particle->isLepton() ) continue; // Only interested in the leptonic decays!
        if (particle->pdgId() > 0){
            nLep++;
            final_V += particle->p4();
        }
        if (particle->pdgId() < 0){
            nAntiLep++;
            final_V += particle->p4();
        }
      }
    }
    if(m_DoSherpattbar){
      if( particle->absPdgId() == 6 ){  //Firstly get the top, this is for getting pTV in Powheg ttbar
        if(particle->status() == 3 ||  particle->status() == 22){
          std::set<int> W_decay_products;
          TLorentzVector temp_final_V = GetDecayProducts(particle, & W_decay_products,false,final_V);

          if( W_decay_products.count(11) || W_decay_products.count(13) || W_decay_products.count(15) ){  //This will get overwritten if more than one leptonic top, but doesn't matter
            final_V = temp_final_V;
          }
        }
      }
    }

  }
  m_VpT = final_V.Pt()*1e-3;
  m_Veta = final_V.Eta();

  //Get met
  CHECK( evtStore()->retrieve(truthMET, "MET_Truth" ) );
  const xAOD::MissingET * met_NonInt = (*truthMET)["NonInt"];
  const xAOD::MissingET * met_Int = (*truthMET)["Int"];
  const xAOD::MissingET * met_IntMuons = (*truthMET)["IntMuons"];
  MET_Vec_Nu.SetPtEtaPhiM(met_NonInt->met()*1e-3,0.0,met_NonInt->phi(),0.0);
  TLorentzVector MET_Vec_Int;
  TLorentzVector MET_Vec_IntMuons;
  MET_Vec_Int.SetPtEtaPhiM(met_Int->met()*1e-3,0.0,met_Int->phi(),0.0);
  MET_Vec_IntMuons.SetPtEtaPhiM(met_IntMuons->met()*1e-3,0.0,met_IntMuons->phi(),0.0);

  // Add the calo. term and the muon term. Has to be done this way or MET_Vec has an artificial inv. mass!
  TVector3 MET_ThreeVec = MET_Vec_Int.Vect() + MET_Vec_IntMuons.Vect();
  MET_Vec.SetVectM(MET_ThreeVec,0.0);
  m_MET = MET_Vec.Pt();

  //get the leptons
  std::vector< const xAOD::TruthParticle* > SignalLeptons;
  const int nLeptonChannel = LeptonSelection(SignalLeptons);
  SaveAllLeptons(SignalLeptons);
  for(unsigned int l = 0; l < SignalLeptons.size(); ++l) {
    m_Lepton_Pt.push_back( SignalLeptons.at(l)->pt()*1e-3 );
    m_Lepton_Eta.push_back( SignalLeptons.at(l)->eta() );
    m_Lepton_Phi.push_back( SignalLeptons.at(l)->phi());
    m_Lepton_Mass.push_back( SignalLeptons.at(l)->m()*1e-3 );
    m_Lepton_ID.push_back( SignalLeptons.at(l)->pdgId() );
  }
  m_nSignalLeptons = nLeptonChannel;
  
  if(m_DoInference){
    //--------------------------------------------------------
    //Modular version
    //njets,MET, Jet1_Pt, Jet1_Mass, Jet2_Pt, Jet2_Mass, Lep1_Pt, Lep2_Pt 
    //m_input_tensor_values;
    m_input_tensor_values["Njets"] = (SignalJets_Vec.size());
    m_input_tensor_values["MET"] = (MET_Vec.Pt());
    //Jet 1 pT and mass
    if (SignalJets_Vec.size()>0){
      m_input_tensor_values["Jet1_Pt"] = (SignalJets_Vec.at(0).Pt());
      m_input_tensor_values["Jet1_Mass"] = (SignalJets_Vec.at(0).M());
    }
    else{
      m_input_tensor_values["Jet1_Pt"] = (0.0);
      m_input_tensor_values["Jet1_Mass"] = (0.0);
    }
    //Jet 2 pT and mass
    if (SignalJets_Vec.size() >1){
      m_input_tensor_values["Jet2_Pt"] = (SignalJets_Vec.at(1).Pt());
      m_input_tensor_values["Jet2_Mass"] = (SignalJets_Vec.at(1).M());
    }
    else{
      m_input_tensor_values["Jet2_Pt"] = (0.0);
      m_input_tensor_values["Jet2_Mass"] = (0.0);
    }
    //m_input_tensor_values[] = (HT);
    //m_input_tensor_values[] = (MET_Vec.Pt());
    if (m_DoSherpaVpT){
      m_input_tensor_values["V_PT"] = (final_V.Pt()*1e-3);
    }
    //else store lepton 1 pT
    else{
    if (SignalLeptons.size()>0 ){
      m_input_tensor_values["Lep1_Pt"] = (SignalLeptons.at(0)->pt()*1e-3);
    }
    //and lepton 2 pT
    else {
      m_input_tensor_values["Lep1_Pt"] = (0.0);
    }
    if (SignalLeptons.size()>1 ){
      m_input_tensor_values["Lep2_Pt"] = (SignalLeptons.at(1)->pt()*1e-3);
    }
    else {
      m_input_tensor_values["Lep2_Pt"] = (0.0);
    }
    }
    std::cout << "input_tensor_values njets " << m_input_tensor_values["Njets"] << std::endl;
    std::cout << "input_tensor_values MET  " << m_input_tensor_values["MET"] << std::endl;
    std::cout << "input_tensor_values jet 1 pt " << m_input_tensor_values["Jet1_Pt"] << std::endl;
    std::cout << "input_tensor_values jet 1 mass " << m_input_tensor_values["Jet1_Mass"] << std::endl;
    std::cout << "input_tensor_values jet 2 pt " << m_input_tensor_values["Jet2_Pt"] << std::endl;
    std::cout << "input_tensor_values jet 2 mass " << m_input_tensor_values["Jet2_Mass"] << std::endl;
    std::cout << "input_tensor_values lep 1 pt " << m_input_tensor_values["Lep1_Pt"] << std::endl;
    std::cout << "input_tensor_values lep 2 pt " << m_input_tensor_values["Lep2_Pt"] << std::endl;
    std::cout << "Hello" <<std::endl;
    m_CARLNN->ValidateMetaAndInputData();
    float weight = m_CARLNN->evaluate();
    std::cout << "Hello 2" <<std::endl;
    m_weight = weight;
    if(m_DoDebug){ ATH_MSG_INFO("Weight w is r^{-1}: "<<weight); }
    //--------------------------------------------------------
    
  }
  setFilterPassed(true); //if got here, assume that means algorithm passed
  Tree->Fill();

  //Clean up of calibracted jets
  //for( xAOD::Jet* jet : SignalJets ){
  //  delete jet;
  //}

  return StatusCode::SUCCESS;
}

StatusCode CARLAthenaAlg::beginInputFile() {
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );


  return StatusCode::SUCCESS;
}

int CARLAthenaAlg::LeptonSelection(std::vector< const xAOD::TruthParticle* > & SignalLeptons) {
  SignalLeptons.clear();

  unsigned int nLooseMuons = 0;
  unsigned int nLooseElectrons = 0;
  std::vector< const xAOD::TruthParticle* > SelMuons;
  std::vector< const xAOD::TruthParticle* > SelElectrons;
  std::vector< const xAOD::TruthParticle* > SelElectronsMuons;
  for(unsigned int m = 0; m < truthMuons->size(); ++m) {

    const xAOD::TruthParticle * muon = (*truthMuons)[m];

    if(PassLooseMuon(muon)) { nLooseMuons++; SelMuons.push_back(muon); SelElectronsMuons.push_back(muon); }

  }
  for(unsigned int e = 0; e < truthElectrons->size(); ++e) {

    const xAOD::TruthParticle * electron = (*truthElectrons)[e];

    if(PassLooseElectron(electron)) { nLooseElectrons++; SelElectrons.push_back(electron); SelElectronsMuons.push_back(electron); }

  }
  if( (nLooseMuons + nLooseElectrons) == 0) {
    //0 lep channel
    return 0;

  } else if( (nLooseMuons + nLooseElectrons) == 1  ) {
    // 1L channel
    if(SelMuons.size() == 1) SignalLeptons = SelMuons;
    if(SelElectrons.size() == 1) SignalLeptons = SelElectrons;
    return 1;

  } else if( nLooseMuons == 2 && nLooseElectrons == 0 ) {
    // 2L channel (muon)
    SignalLeptons = SelMuons;
    return 2;

  }  else if( nLooseElectrons == 2 && nLooseMuons == 0 ) {

    // 2L channel (electron)
    SignalLeptons = SelElectrons;
    return 2;

  } else if( nLooseMuons == 1 && nLooseElectrons == 1 ) {
    // 2L channel (e/mu)
    SignalLeptons = SelElectronsMuons;
    return 2;

  } else {

    return -1;

  }

}

bool CARLAthenaAlg::PassLooseMuon(const xAOD::TruthParticle * mu) {

  if( fabs(mu->eta()) > 2.5 ) return false;

  if( mu->pt() < 25e3 ) return false;

      return true;
  
}
bool CARLAthenaAlg::PassLooseElectron(const xAOD::TruthParticle * el) {

  if( fabs(el->eta()) > 2.47 ) return false;

  if( el->pt() < 27e3  ) return false;

  return true;

}
void CARLAthenaAlg::SaveAllLeptons(std::vector< const xAOD::TruthParticle* > & SignalLeptons){

  SignalLeptons.clear();

  for(unsigned int m = 0; m < truthMuons->size(); ++m) {
    const xAOD::TruthParticle * muon = (*truthMuons)[m];
    SignalLeptons.push_back(muon);
  }

  for(unsigned int e = 0; e < truthElectrons->size(); ++e) {
    const xAOD::TruthParticle * electron = (*truthElectrons)[e];
    SignalLeptons.push_back(electron);
  }

}

