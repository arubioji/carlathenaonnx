#See: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake for more details about anything here
from AthenaCommon import CfgMgr
from AthenaCommon.AthenaCommonFlags import jobproperties as jp

maxEVNT=vars().get('EVTMAX', -1)
jp.AthenaCommonFlags.EvtMax.set_Value_and_Lock( maxEVNT )
theApp.EvtMax=maxEVNT

import AthenaPoolCnvSvc.ReadAthenaPool                   #sets up reading of POOL files (e.g. xAODs)

#testFile=["/afs/cern.ch/work/m/mvesterb/public/data/EVNT.13727623._000073.pool.root.1"]
svcMgr.EventSelector.InputCollections=["/afs/cern.ch/work/s/sjiggins/CARL_Athena/carlathena_AthDerivation_Onnx_SLC6_MetaData/run/Inputs/ttbar_EVNT.20196029._002677.pool.root.1"]
#svcMgr.EventSelector.InputCollections=["/afs/cern.ch/work/m/mvesterb/public/data/EVNT.09188493._003226.pool.root.1"]
#svcMgr.EventSelector.InputCollections=["/afs/cern.ch/work/s/sjiggins/CARL_Athena/carlathena_AthAnalysis/run/EVNT.09188493._003226.pool.root.1"]

from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
athenaCommonFlags.FilesInput = svcMgr.EventSelector.InputCollections
from AthenaCommon.AppMgr import ToolSvc

import os.path
if(os.path.isfile(svcMgr.EventSelector.InputCollections[0])):
  print("File exists!!!!")
else:
  print("File do not exist!!!!")
  print(svcMgr.EventSelector.InputCollections[0])

from PyUtils import AthFile
af = AthFile.fopen(svcMgr.EventSelector.InputCollections[0]) #opens the first file from the InputCollections list
mc_channel_number = af.run_number[0]

print mc_channel_number


# Create the algorithm's configuration.
#from AnaAlgorithm.DualUseConfig import createAlgorithm
#alg = createAlgorithm ( 'CARLAthenaAlg', 'AnalysisAlg' )

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the main alg sequence
algseq = CfgMgr.AthSequencer("AthAlgSeq")                #gets the main AthSequencer
# Convert the EVNT truth into xAOD truth
algseq += CfgMgr.xAODMaker__xAODTruthCnvAlg("GEN_EVNT2FLAT",AODContainerName="GEN_EVENT",WriteTruthMetaData=False)
algseq += CfgMgr.xAODMaker__EventInfoCnvAlg()


# create the output streams
if not hasattr(svcMgr, 'THistSvc'): svcMgr += CfgMgr.THistSvc()
#svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += [ "test DATAFILE='"+"test.root' OPT='RECREATE'"]

# Truth Jets
from JetRec.JetRecStandard import jtm
from JetRec.JetRecConf import JetAlgorithm
from CARLAthena.MCTruthCommon_JO_Manual import *
initialiseJetAlgs(algseq, CfgMgr, ToolSvc)

akt4 = jtm.addJetFinder("AntiKt4TruthJets", "AntiKt", 0.4, "truth", modifiersin=[jtm.truthpartondr, jtm.partontruthlabel, jtm.removeconstit, jtm.jetdrlabeler, jtm.trackjetdrlabeler], ptmin= 20000)
akt4alg = JetAlgorithm("jetalgAntiKt4TruthJets", Tools = [akt4] )
algseq += akt4alg

# Truth MET
import METReconstruction.METConfig_Truth
from METReconstruction.METRecoFlags import metFlags # not sure if you even need this line
from METReconstruction.METRecoConfig import getMETRecoAlg
metAlg = getMETRecoAlg('METReconstruction')
algseq += metAlg

from DerivationFrameworkMCTruth.DerivationFrameworkMCTruthConf import DerivationFramework__TruthCollectionMaker
DerivationFrameworkSimBarcodeOffset = int(200e3)
DFCommonTruthMuonTool = DerivationFramework__TruthCollectionMaker(name                   = "DFCommonTruthMuonTool",
                                                                  NewCollectionName       = "TruthMuons",
                                                                  ParticleSelectionString = "(abs(TruthParticles.pdgId) == 13) && (TruthParticles.status == 1) && TruthParticles.barcode < "+str(DerivationFrameworkSimBarcodeOffset))

ToolSvc += DFCommonTruthMuonTool
DFCommonTruthElectronTool = DerivationFramework__TruthCollectionMaker(name               = "DFCommonTruthElectronTool",
                                                                      NewCollectionName       = "TruthElectrons",
                                                                      ParticleSelectionString = "(abs(TruthParticles.pdgId) == 11) && (TruthParticles.status == 1) && TruthParticles.barcode < "+str(DerivationFrameworkSimBarcodeOffset))

ToolSvc += DFCommonTruthElectronTool

augmentationToolsList = [DFCommonTruthMuonTool,DFCommonTruthElectronTool]

from DerivationFrameworkCore.DerivationFrameworkCoreConf import DerivationFramework__CommonAugmentation

algseq += CfgMgr.DerivationFramework__CommonAugmentation("MCTruthCommonKernel",AugmentationTools = augmentationToolsList)



# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
#alg = createAlgorithm ( 'CARLAthenaAlg', 'AnalysisAlg' )
alg = CfgMgr.CARLAthenaAlg( DoInference = True,
                            DoDebug = True,
                            DoSherpattbar=True,
                            DoSherpaVpT=False)
algseq += alg

# limit the number of events (for testing purposes)
theApp.EvtMax = 20

#algseq += CfgMgr.MyAnalysis()
# optional include for reducing printout from athena
#include("AthAnalysisBaseComps/SuppressLogging.py")
#include("CARLAthena/SuppressLogging.py")
